﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Initiation.Shared.Dto.Input.Instrument.ForUpdate
{
    /// <summary>
    /// Dto input Instrument for update
    /// </summary>
    public class DtoInputInstrumentForUpdate
    {
        /// <summary>
        /// Instrument name
        /// </summary>
        [Required(ErrorMessage = "Le champ « {0} » est obligatoire")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Le champ « {0} » doit faire plus de 2 caractères et maximum 40 caractères")]
        [DisplayName("Nom")]
        public string Name { get; set; }

        /// <summary>
        /// Number of strings
        /// </summary>
        public int Strings { get; set; }

        /// <summary>
        /// Year of manufacture
        /// </summary>
        public int YearManufacture { get; set; }
    }
}
