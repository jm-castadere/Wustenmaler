﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Initiation.Shared.Dto.Input.User.ForRegister
{
    /// <summary>
    /// Dto for model User
    /// </summary>
    public class DtoInputUserForRegister
    {
        private string _Username;
        /// <summary>
        /// Username
        /// </summary>
        [Required(ErrorMessage = "Le champ « {0} » est obligatoire.")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Vous devez spécifier un nom d'utilisateur entre 2 et 30 caractères")]
        [Display(Name = "Nom d'utilisateur")]
        public string Username
        {
            get { return _Username; }
            set
            {
                // Set "Value"
                _Username = value;
                // Raise event for value changed
                OnValueChanged(null);
            }
        }

        /// <summary>
        /// Password
        /// </summary>
        [Required(ErrorMessage = "Le champ « {0} » est obligatoire.")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Vous devez spécifier un mot de passe entre 4 et 30 caractères")]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }

        /// <summary>
        /// Event
        /// </summary>
        public event EventHandler ValueChanged;

        /// <summary>
        /// On value changed
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected virtual void OnValueChanged(EventArgs e)
        {
            // Raise the event
            if (ValueChanged != null)
                ValueChanged(this, e);
        }
    }
}
