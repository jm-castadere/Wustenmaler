﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Initiation.Shared.Dto.Input.User.ForLogin
{
    /// <summary>
    /// Dto for model User
    /// </summary>
    public class DtoInputUserForLogin
    {
        /// <summary>
        /// Username
        /// </summary>
        [Required(ErrorMessage = "Le champ « {0} » est obligatoire.")]
        [Display(Name = "Nom d'utilisateur")]
        public string Username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required(ErrorMessage = "Le champ « {0} » est obligatoire.")]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }
    }
}
