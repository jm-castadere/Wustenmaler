﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Initiation.Shared.Dto.Input.User.ForAvailable
{
    /// <summary>
    /// Dto for model User
    /// </summary>
    public class DtoInputUserForAvailable
    {
        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }
    }
}
