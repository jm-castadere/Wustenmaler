﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Initiation.Shared.Dto.Output.Instrument.ForList
{
    /// <summary>
    /// Dto output Instrument for list
    /// </summary>
    public class DtoOutputInstrumentForList
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name of instrument
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Number of strings
        /// </summary>
        public int Strings { get; set; }
    }
}
