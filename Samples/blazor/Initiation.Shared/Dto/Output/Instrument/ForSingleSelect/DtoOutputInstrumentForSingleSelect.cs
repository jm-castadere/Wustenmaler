﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Initiation.Shared.Dto.Output.Instrument.ForSingleSelect
{
    /// <summary>
    /// Dto output Instrument for single select
    /// </summary>
    public class DtoOutputInstrumentForSingleSelect
    {
        /// <summary>
        /// Name of instrument
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Number of strings
        /// </summary>
        public int Strings { get; set; }
    }
}
