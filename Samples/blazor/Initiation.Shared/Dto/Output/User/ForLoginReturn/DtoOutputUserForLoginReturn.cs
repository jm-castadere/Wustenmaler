﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Initiation.Shared.Dto.Output.User.ForLoginReturn
{
    /// <summary>
    /// Dto output on login
    /// </summary>
    public class DtoOutputUserForLoginReturn
    {
        /// <summary>
        /// Token JWT
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }
    }
}
