﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Initiation.Shared.Helpers
{
    /// <summary>
    /// Header for pagination
    /// </summary>
    public class PaginationHeader
    {
        /// <summary>
        /// Current page
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Items per page
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Total items
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Total pages
        /// </summary>
        public int TotalPages { get; set; }
    }
}
