﻿using Initiation.Client.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Initiation.Client.Services.Js
{
    /// <summary>
    /// Alertify javascript service
    /// https://alertifyjs.com
    /// </summary>
    public interface IJsAlertifyService
    {
        Task Open(string message, TypeAlertify type);
    }
}
