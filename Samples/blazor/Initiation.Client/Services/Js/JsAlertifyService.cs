﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Initiation.Client.Helpers;
using Microsoft.JSInterop;

namespace Initiation.Client.Services.Js
{
    /// <summary>
    /// Javascript service for alertify
    /// https://alertifyjs.com
    /// </summary>
    public class JsAlertifyService : IJsAlertifyService
    {
        /// <summary>
        /// Javascript runtime
        /// </summary>
        private readonly IJSRuntime _jsRuntime;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="jsRuntime">Javascript runtime</param>
        public JsAlertifyService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task Open(string message, TypeAlertify type)
        {
            switch (type)
            {
                case TypeAlertify.Success:
                    await _jsRuntime.InvokeAsync<bool>("Alertify", message, "success", 5);
                    break;
                case TypeAlertify.Warning:
                    await _jsRuntime.InvokeAsync<bool>("Alertify", message, "warning", 5);
                    break;
                case TypeAlertify.Error:
                    await _jsRuntime.InvokeAsync<bool>("Alertify", message, "error", 5);
                    break;
                default:
                    await _jsRuntime.InvokeAsync<bool>("Alertify", message);
                    break;
            }
        }
    }
}
