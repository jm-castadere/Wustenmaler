﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Initiation.Client.Services.Http
{
    /// <summary>
    ///  Http weather service
    /// </summary>
    public class HttpWeatherService : IHttpWeatherService
    {
        /// <summary>
        /// Http client
        /// </summary>
        private readonly HttpClient _httpClient;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="httpClient">Http client</param>
        public HttpWeatherService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// Get weather forcasts
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetWeatherForcasts()
        {
            var req = new HttpRequestMessage(HttpMethod.Get, $"{Constants.URL_BASE}api/SampleData/WeatherForecasts");
            return await _httpClient.SendAsync(req);
        }
    }
}
