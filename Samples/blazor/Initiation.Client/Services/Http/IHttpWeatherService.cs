﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Initiation.Client.Services.Http
{
    /// <summary>
    /// Http weather service
    /// </summary>
    public interface IHttpWeatherService
    {
        /// <summary>
        /// Get weather forcasts
        /// </summary>
        /// <returns></returns>
        Task<HttpResponseMessage> GetWeatherForcasts();
    }
}
