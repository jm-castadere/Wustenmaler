﻿using Initiation.Shared.Dto.Input.User.ForAvailable;
using Initiation.Shared.Dto.Input.User.ForLogin;
using Initiation.Shared.Dto.Input.User.ForRegister;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Initiation.Client.Services.Http
{
    /// <summary>
    /// User service (authentification)
    /// </summary>
    public interface IHttpUserService
    {
        /// <summary>
        /// Register
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        Task<HttpResponseMessage> PostRegister(DtoInputUserForRegister dto);

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        Task<HttpResponseMessage> PostLogin(DtoInputUserForLogin dto);

        /// <summary>
        /// Available
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        Task<Boolean> PostAvailable(DtoInputUserForAvailable dto);
    }
}
