﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Initiation.Client.Services.Http
{
    /// <summary>
    /// Http instrument service
    /// </summary>
    public interface IHttpInstrumentService
    {
        /// <summary>
        /// Get instruments by page
        /// </summary>
        /// <param name="page">Page</param>
        /// <returns></returns>
        Task<HttpResponseMessage> GetInstrumentsPaged(int page);
    }
}
