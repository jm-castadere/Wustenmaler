﻿using Cloudcrate.AspNetCore.Blazor.Browser.Storage;
using Initiation.Shared.Dto.Input.User.ForAvailable;
using Initiation.Shared.Dto.Input.User.ForLogin;
using Initiation.Shared.Dto.Input.User.ForRegister;
using Initiation.Shared.Dto.Output.User.ForLoginReturn;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Client.Services.Http
{
    /// <summary>
    /// Http User service (authentification)
    /// </summary>
    public class HttpUserService : IHttpUserService
    {
        /// <summary>
        /// HttpClient
        /// </summary>
        private readonly HttpClient _httpClient;
        /// <summary>
        /// LocalStorage
        /// </summary>
        private readonly LocalStorage _storage;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="httpClient">HttpClient</param>
        /// <param name="storage">LocalStorage</param>
        public HttpUserService(HttpClient httpClient, LocalStorage storage)
        {
            _httpClient = httpClient;
            _storage = storage;
        }

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PostRegister(DtoInputUserForRegister dto)
        {
            var requestJson = JsonConvert.SerializeObject(dto);
            using (var req = new HttpRequestMessage(HttpMethod.Post, $"{Constants.URL_BASE}api/User/register"))
            {
                req.Content = new StringContent(requestJson, Encoding.Default, "application/json");
                return await _httpClient.SendAsync(req);
            }
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PostLogin(DtoInputUserForLogin dto)
        {
            _storage.RemoveItem("token");
            _storage.RemoveItem("username");
            var requestJson = JsonConvert.SerializeObject(dto);
            using (var req = new HttpRequestMessage(HttpMethod.Post, $"{Constants.URL_BASE}api/User/login"))
            {
                req.Content = new StringContent(requestJson, Encoding.Default, "application/json");
                var response = await _httpClient.SendAsync(req);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // Set localstorage
                    string content = await response.Content.ReadAsStringAsync();
                    var _dto = JsonConvert.DeserializeObject<DtoOutputUserForLoginReturn>(content);
                    _storage["token"] = _dto.Token;
                    _storage["username"] = _dto.Username;
                }
                return response;
            }
        }

        /// <summary>
        /// Available
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        public async Task<Boolean> PostAvailable(DtoInputUserForAvailable dto)
        {
            var swAvailable = false;
            if (dto.Username == "" || dto.Username.Length <= 2 || dto.Username.Length > 30)
                return swAvailable;
            var requestJson = JsonConvert.SerializeObject(dto);
            using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, $"{Constants.URL_BASE}api/User/available"))
            {
                req.Content = new StringContent(requestJson, Encoding.Default, "application/json");
                using (var response = await _httpClient.SendAsync(req))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                        swAvailable = Convert.ToBoolean(await response.Content.ReadAsStringAsync());
                }
            }
            return swAvailable;
        }
    }
}
