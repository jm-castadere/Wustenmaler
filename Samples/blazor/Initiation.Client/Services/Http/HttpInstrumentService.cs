﻿using Cloudcrate.AspNetCore.Blazor.Browser.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Initiation.Client.Services.Http
{
    /// <summary>
    /// Http Instrument service
    /// </summary>
    public class HttpInstrumentService : IHttpInstrumentService
    {
        /// <summary>
        /// Http client
        /// </summary>
        private readonly HttpClient _httpClient;
        private readonly LocalStorage _storage;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="httpClient">Http client</param>
        public HttpInstrumentService(HttpClient httpClient, LocalStorage storage)
        {
            _httpClient = httpClient;
            _storage = storage;
        }

        /// <summary>
        /// Get instruments by page
        /// </summary>
        /// <param name="page">Page</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetInstrumentsPaged(int page)
        {
            var req = new HttpRequestMessage(HttpMethod.Get, $"{Constants.URL_BASE}api/Instrument/Paged?pageNumber={page}");
            // req.Headers.Add("Authorization", $"Bearer {_storage["token"]}");
            return await _httpClient.SendAsync(req);
        }
    }
}
