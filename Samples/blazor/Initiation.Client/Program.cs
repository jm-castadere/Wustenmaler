﻿using Microsoft.AspNetCore.Blazor.Hosting;

namespace Initiation.Client
{
    static class Constants
    {
        public const string URL_BASE = "http://localhost:55635/";
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IWebAssemblyHostBuilder CreateHostBuilder(string[] args) =>
            BlazorWebAssemblyHost.CreateDefaultBuilder()
                .UseBlazorStartup<Startup>();
    }
}
