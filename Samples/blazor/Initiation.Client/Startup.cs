using Cloudcrate.AspNetCore.Blazor.Browser.Storage;
using Initiation.Client.Services.Http;
using Initiation.Client.Services.Js;
using Microsoft.AspNetCore.Blazor.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Initiation.Client
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IUriHelper>(sp => WebAssemblyUriHelper.Instance);
            // Http services
            services.AddSingleton<IHttpWeatherService, HttpWeatherService>();
            services.AddSingleton<IHttpInstrumentService, HttpInstrumentService>();
            services.AddSingleton<IHttpUserService, HttpUserService>();
            // Javascript services
            services.AddSingleton<IJsAlertifyService, JsAlertifyService>();
            // Localstorage
            services.AddStorage();
        }

        public void Configure(IComponentsApplicationBuilder app)
        {
            app.AddComponent<App>("app");
        }
    }
}
