﻿using Initiation.Server.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Initiation.Server.Data
{
    /// <summary>
    /// DataContext
    /// </summary>
    public class DataContext: DbContext
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="options">Options</param>
        public DataContext(DbContextOptions<DataContext> options) : base (options) { }

        public DbSet<Instrument> Instruments { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
