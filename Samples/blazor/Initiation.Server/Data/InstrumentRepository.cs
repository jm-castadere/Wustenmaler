﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Initiation.Server.Helpers;
using Initiation.Server.Helpers.PagedParams;
using Initiation.Server.Models;
using Microsoft.EntityFrameworkCore;

namespace Initiation.Server.Data
{
    /// <summary>
    /// Instrument repository
    /// </summary>
    public class InstrumentRepository : IInstrumentRepository
    {
        /// <summary>
        /// DataContext
        /// </summary>
        private readonly DataContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">DataContext</param>
        public InstrumentRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Add entity
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="entity">Entity</param>
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        /// <summary>
        /// Update entity
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="entity">Entity</param>
        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }

        /// <summary>
        /// Delete entity
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="entity">Entity</param>
        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        /// <summary>
        /// Save change
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// Get list of instruments
        /// </summary>
        /// <returns></returns>
        public async Task<List<Instrument>> GetInstruments()
        {
            return await _context.Instruments.OrderBy(x => x.Name).ToListAsync();
        }

        /// <summary>
        /// Get list paged of instruments
        /// </summary>
        /// <param name="param">Instrument params</param>
        /// <returns></returns>
        public async Task<PagedList<Instrument>> GetInstrumentsPaged(InstrumentParams param)
        {
            var items = _context.Instruments.OrderBy(x => x.Name).AsQueryable();
            return await PagedList<Instrument>.CreateAsync(items, param.PageNumber, param.PageSize);
        }

        /// <summary>
        /// Get Instrument by primary key
        /// </summary>
        /// <param name="id">Instrument primary key</param>
        /// <returns></returns>
        public async Task<Instrument> GetInstrument(int id)
        {
            return await _context.Instruments.Where(x => x.Id == id).FirstOrDefaultAsync();
        }
    }
}
