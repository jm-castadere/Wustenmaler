﻿using Initiation.Server.Helpers;
using Initiation.Server.Helpers.PagedParams;
using Initiation.Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Initiation.Server.Data
{
    /// <summary>
    /// Instrument repository
    /// </summary>
    public interface IInstrumentRepository
    {
        /// <summary>
        /// Add entity
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="entity">Entity</param>
        void Add<T>(T entity) where T : class;

        /// <summary>
        /// Update entity
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="entity">Entity</param>
        void Update<T>(T entity) where T : class;

        /// <summary>
        /// Delete entity
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="entity">Entity</param>
        void Delete<T>(T entity) where T : class;

        /// <summary>
        /// Save change
        /// </summary>
        /// <returns></returns>
        Task<bool> SaveAll();

        /// <summary>
        /// Get list of instruments
        /// </summary>
        /// <returns></returns>
        Task<List<Instrument>> GetInstruments();

        /// <summary>
        /// Get list paged of instruments
        /// </summary>
        /// <param name="param">Instrument params</param>
        /// <returns></returns>
        Task<PagedList<Instrument>> GetInstrumentsPaged(InstrumentParams param);

        /// <summary>
        /// Get instrument by primary key
        /// </summary>
        /// <param name="id">Instrument primary key</param>
        /// <returns></returns>
        Task<Instrument> GetInstrument(int id);
    }
}
