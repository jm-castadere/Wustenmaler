﻿using Initiation.Server.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Server.Data
{
    /// <summary>
    /// Seed Datas
    /// </summary>
    public class Seed
    {
        /// <summary>
        /// DataContext
        /// </summary>
        private readonly DataContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">DataContext</param>
        public Seed(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Seed Instruments
        /// </summary>
        public void SeedInstruments()
        {
            var instrumentsData =
                System.IO.File.ReadAllText("Data/Seed/InstrumentSeed.json", Encoding.GetEncoding("iso-8859-1"));
            var instruments = JsonConvert.DeserializeObject<List<Instrument>>(instrumentsData);
            foreach (var instrument in instruments)
            {
                if (! _context.Instruments.Any(x => x.Name == instrument.Name))
                {
                    _context.Instruments.Add(instrument);
                }
            }
            _context.SaveChanges();
        }
    }
}
