﻿using Initiation.Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Initiation.Server.Data
{
    /// <summary>
    /// User repository (authentification)
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        Task<User> Login(string username, string password);

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="user">Model user</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        Task<User> Register(User user, string password);

        /// <summary>
        /// Method if username exist in User model
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns></returns>
        Task<bool> UserExists(string username);
    }
}
