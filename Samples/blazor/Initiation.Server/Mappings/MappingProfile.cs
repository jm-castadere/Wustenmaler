﻿using AutoMapper;
using Initiation.Server.Models;
using Initiation.Shared.Dto.Output.Instrument.ForList;
using Initiation.Shared.Dto.Output.Instrument.ForSingleSelect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Initiation.Server.Mappings
{
    /// <summary>
    /// Automapper default profile
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Instrument, DtoOutputInstrumentForList>();
            CreateMap<Instrument, DtoOutputInstrumentForSingleSelect>();
        }
    }
}
