﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Initiation.Server.Models
{
    /// <summary>
    /// Model User
    /// </summary>
    public class User
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Password hash sha512
        /// </summary>
        public byte[] PasswordHash { get; set; }

        /// <summary>
        /// Password salt sha512
        /// </summary>
        public byte[] PasswordSalt { get; set; }

        /// <summary>
        /// Date created
        /// </summary>
        public DateTime Created { get; set; }
    }
}
