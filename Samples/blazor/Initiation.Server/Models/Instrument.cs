﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Initiation.Server.Models
{
    /// <summary>
    /// Model Instrument
    /// </summary>
    public class Instrument
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Instrument name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Number of strings
        /// </summary>
        public int Strings { get; set; }
        /// <summary>
        /// Year of manufacture
        /// </summary>
        public int YearManufacture { get; set; }
    }
}
