﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Initiation.Server.Data;
using Initiation.Server.Helpers;
using Initiation.Server.Helpers.PagedParams;
using Initiation.Server.Models;
using Initiation.Shared.Dto.Input.Instrument.ForCreate;
using Initiation.Shared.Dto.Input.Instrument.ForUpdate;
using Initiation.Shared.Dto.Output.Instrument.ForList;
using Initiation.Shared.Dto.Output.Instrument.ForSingleSelect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace Initiation.Server.Controllers
{
    /// <summary>
    /// Instrument controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [OpenApiTag("Instrument", Description = "Instrument controller")]
    public class InstrumentController : ControllerBase
    {
        /// <summary>
        /// Instrument repository
        /// </summary>
        private readonly IInstrumentRepository _repo;
        /// <summary>
        /// Automapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repo">Instrument repository</param>
        /// <param name="mapper">Automapper</param>
        public InstrumentController(IInstrumentRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        /// <summary>
        /// Get list of instruments
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [SwaggerResponse(HttpStatusCode.OK, typeof(List<DtoOutputInstrumentForList>), Description = "Ok")]
        public async Task<IActionResult> GetInstruments()
        {
            var items = await _repo.GetInstruments();
            var itemsDto = _mapper.Map<List<DtoOutputInstrumentForList>>(items);
            return Ok(itemsDto);
        }

        /// <summary>
        /// Get list paged of Instruments
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet("Paged")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(List<DtoOutputInstrumentForList>), Description = "Ok")]
        public async Task<IActionResult> GetInstrumentsPaged([FromQuery] InstrumentParams param)
        {
            var items = await _repo.GetInstrumentsPaged(param);
            Response.AddPagination(items.CurrentPage, items.PageSize, items.TotalCount, items.TotalPages);
            return Ok(items);
        }

        /// <summary>
        /// Get one instrument by primary key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(DtoOutputInstrumentForSingleSelect), Description = "Ok")]
        public async Task<IActionResult> GetInstrument(int id)
        {
            var item = await _repo.GetInstrument(id);
            var itemDto = _mapper.Map<DtoOutputInstrumentForSingleSelect>(item);
            return Ok(itemDto);
        }

        /// <summary>
        /// Delete Instrument by primary key
        /// </summary>
        /// <param name="id">Instrument primary key</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(void), Description = "Ok")]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(string), Description = "Impossible d'effacer l'instrument, il n'existe pas")]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(string), Description = "Impossible d'effacer l'instrument")]
        public async Task<IActionResult> DeleteInstrument(int id)
        {
            var item = await _repo.GetInstrument(id);
            if (item == null)
            {
                return BadRequest("Impossible d'effacer l'instrument, il n'existe pas");
            }
            _repo.Delete(item);
            if (await _repo.SaveAll())
            {
                return Ok();
            }
            else
            {
                return BadRequest("Impossible d'effacer l'instrument");
            }
        }

        /// <summary>
        /// Create instrument
        /// </summary>
        /// <param name="dto">Dto input for Instrument</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, typeof(void), Description = "Ok")]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(string), Description = "Impossible de créer cet instrument")]
        public async Task<IActionResult> CreateInstrument(DtoInputInstrumentForCreate dto)
        {
            // Prepare model
            var item = new Instrument
            {
                Name = dto.Name,
                Strings = dto.Strings,
                YearManufacture = dto.YearManufacture
            };

            _repo.Add(item);

            if (!await _repo.SaveAll())
                return BadRequest("Impossible de créer cet instrument");
            return Ok();
        }

        /// <summary>
        /// Update instrement by primary key
        /// </summary>
        /// <param name="id">Primary key Insutrement</param>
        /// <param name="dto">Dto for model Instrument</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(void), Description = "Ok")]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(string), Description = "Impossible de modifier cet instrument, il n'existe pas")]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(string), Description = "Impossible de modifier cet instrument")]
        public async Task<IActionResult> UpdateInstrument(int id, DtoInputInstrumentForUpdate dto)
        {
            var item = await _repo.GetInstrument(id);
            if (item == null)
                return BadRequest("Impossible de modifier cet instrument, il n'existe pas");
            // Prepare model
            item.Name = dto.Name;
            item.Strings = dto.Strings;
            item.YearManufacture = dto.YearManufacture;

            _repo.Update(item);

            if (!await _repo.SaveAll())
                return BadRequest("Impossible de modifier cet insutrement");
            return Ok();
        }
    }
}