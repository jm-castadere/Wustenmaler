﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Initiation.Server.Data;
using Initiation.Server.Models;
using Initiation.Shared.Dto.Input.User.ForAvailable;
using Initiation.Shared.Dto.Input.User.ForLogin;
using Initiation.Shared.Dto.Input.User.ForRegister;
using Initiation.Shared.Dto.Output.User.ForLoginReturn;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using NSwag.Annotations;

namespace Initiation.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [OpenApiTag("User", Description = "User controller (authentification)")]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// User repository
        /// </summary>
        private readonly IUserRepository _repo;

        /// <summary>
        /// Configuration
        /// </summary>
        private readonly IConfiguration _config;

        /// <summary>
        /// TextInfo
        /// </summary>
        private static TextInfo tinfo = CultureInfo.CurrentCulture.TextInfo;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repo">User repository</param>
        /// <param name="config">Configuration</param>
        public UserController(IUserRepository repo, IConfiguration config)
        {
            _repo = repo;
            _config = config;
        }

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        [HttpPost("register")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(void), Description = "Ok")]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(string), Description="L'utilisateur « Nom » existe déjà")]
        public async Task<IActionResult> Register(DtoInputUserForRegister dto)
        {
            dto.Username = dto.Username.ToLower();
            if (await _repo.UserExists(dto.Username))
            {
                return BadRequest($"L'utilisateur « { tinfo.ToTitleCase(dto.Username) } » existe déjà");
            }
            var userToCreate = new User
            {
                Username = dto.Username
            };
            await _repo.Register(userToCreate, dto.Password);
            return Ok();
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        [HttpPost("login")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(DtoOutputUserForLoginReturn), Description = "Ok")]
        [SwaggerResponse(HttpStatusCode.Unauthorized, typeof(string), Description = "Pas autorisé à se connecter")]
        public async Task<IActionResult> Login(DtoInputUserForLogin dto)
        {
            var userFromRepo = await _repo.Login(dto.Username, dto.Password);
            if (userFromRepo == null)
                return Unauthorized("Pas autorisé à se connecter");
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                new Claim(ClaimTypes.Name, userFromRepo.Username)
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var loginDto = new DtoOutputUserForLoginReturn
            {
                Token = tokenHandler.WriteToken(token),
                Username = userFromRepo.Username
            };
            return Ok(loginDto);
        }

        /// <summary>
        /// Username available
        /// </summary>
        /// <param name="dto">Dto</param>
        /// <returns></returns>
        [HttpPost("available")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(Boolean), Description = "Ok")]
        public async Task<IActionResult> Available(DtoInputUserForAvailable dto)
        {
            var swAvailable = await _repo.UserExists(dto.Username);
            return Ok(!swAvailable);
        }
    }
}