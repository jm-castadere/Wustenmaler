﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Initiation.Server.Helpers.PagedParams
{
    /// <summary>
    /// Pagination params for Instrument
    /// </summary>
    public class InstrumentParams
    {
        /// <summary>
        /// Maximum page size
        /// </summary>
        private const int MaxPageSize = 50;
        /// <summary>
        /// Page number
        /// </summary>
        public int PageNumber { get; set; } = 1;
        /// <summary>
        /// Page size
        /// </summary>
        private int pageSize = 5;
        /// <summary>
        /// Page size
        /// </summary>
        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = (value > MaxPageSize) ? MaxPageSize : value; }
        }
    }
}
