﻿using JMC.BlazorServer.Domain.Models;

namespace JMC.BlazorServer.Service.Interfaces
{
    public interface IWeatherForecastService
    {
        Task<WeatherForecast[]> GetForecastAsync(DateTime startDate);
    }
}