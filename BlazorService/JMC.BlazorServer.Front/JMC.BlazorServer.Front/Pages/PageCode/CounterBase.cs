﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace JMC.BlazorServer.Front.Pages.PageCode
{
    public class CounterBase : ComponentBase
    {

        protected int currentCount = 0;


        [Parameter]
        public int InitCount { get; set; }


        protected bool IsLoading { get; private set; }

        protected string SetColor { get; private set; }

        /// <summary>
        /// Paramter optional
        /// </summary>
        /// <param name="e"></param>
        protected void IncrementCount(MouseEventArgs e)
        {
            if (e.AltKey)
            {
                currentCount += 2;
            }
            else
            {
                currentCount++;

            }


            if (currentCount >= 10)
            {
                SetColor = "red";
            }
            else
            {
                SetColor = "Green";
            }

        }

    }
}
