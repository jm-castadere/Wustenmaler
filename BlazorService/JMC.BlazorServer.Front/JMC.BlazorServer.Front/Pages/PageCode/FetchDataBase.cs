﻿using JMC.BlazorServer.Domain.Models;
using JMC.BlazorServer.Service.Interfaces;
using Microsoft.AspNetCore.Components;

namespace JMC.BlazorServer.Front.Pages.PageCode
{
    public class FetchDataBase : ComponentBase
    {

        [Inject]
        IWeatherForecastService ForecastService { get; set; }


        protected WeatherForecast[]? forecasts;

        protected override async Task OnInitializedAsync()
        {
            forecasts = await ForecastService.GetForecastAsync(DateTime.Now);
        }

    }
}
