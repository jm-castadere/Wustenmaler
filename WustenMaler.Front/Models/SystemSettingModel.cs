﻿namespace WustenMaler.Front.Models
{
    public class SystemSettingModel
    {
        public string Version { get; set; }
        public bool IsActiv { get; set; }
        public int Timeout { get; set; }

    }
}
