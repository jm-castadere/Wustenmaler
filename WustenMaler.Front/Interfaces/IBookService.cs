﻿using System.Collections.Generic;
using WustenMaler.Front.Services;

namespace WustenMaler.Front.Interfaces
{
    public interface IBookService
    {
        IEnumerable<Book> MostPopular(int numberToTake);
    }
}
