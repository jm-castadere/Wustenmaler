﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Diagnostics;
using WustenMaler.Front.Models;

namespace WustenMaler.Front.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IOptions<SystemSettingModel> _settings;

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration, IOptions<SystemSettingModel> settings)
        {
            _logger = logger;
            _configuration = configuration;
            _settings = settings;
        }

        public IActionResult Index()
        {
            ViewBag.SystemeValue = "value appsetting of model->"+_settings.Value.Version;
         
            //Value config add in  builder.AddJsonFile("parameters.json");
            ViewBag.ValParam ="value  paramsetting builder->"+  _configuration["Param"];

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
