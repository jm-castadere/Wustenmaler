﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WustenMaler.Blazor.Server.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
