﻿namespace WusMalBlazorServer.Repository.Services
{
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using WusMalBlazorServer.Domain.DTOs;
    using WusMalBlazorServer.Domain.Interfaces.Repositories;
    using WusMalBlazorServer.Repository.Contexts;


    public class BlazorServerRepository : IBlazorServerRepository
    {
        private readonly BlazorServerDbContext context;
        private readonly IMapper mapper;
        public BlazorServerRepository(BlazorServerDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        public async Task<HotelRoomDto> IsUnique(string name)
        {
            try
            {
                Domain.Entities.HotelRoom room = await context.HotelRooms.FirstOrDefaultAsync(r => r.Name == name);
                return mapper.Map<Domain.Entities.HotelRoom, HotelRoomDto>(room);
            }
            catch
            {
                return null;
            }
        }

        public async Task<HotelRoomDto> CreateHotelRoom(HotelRoomDto r)
        {
            var room = mapper.Map<HotelRoomDto, Domain.Entities.HotelRoom>(r);

            room.CreatedDate = DateTime.UtcNow;

            room.CreatedBy = WindowsIdentity.GetCurrent().Name;

            var added = await context.AddAsync(room);
            await context.SaveChangesAsync();

            return mapper.Map<Domain.Entities.HotelRoom, HotelRoomDto>(room);
        }

        public IEnumerable<HotelRoomDto> GetAllHotelRooms()
        {
            try
            {
                IEnumerable<HotelRoomDto> rooms =
                    mapper.Map<IEnumerable<Domain.Entities.HotelRoom>, IEnumerable<HotelRoomDto>>(context.HotelRooms);

                return rooms;
            }
            catch
            {
                return null;
            }
        }

        public async Task<HotelRoomDto> GetHotelRoom(int id)
        {
            try
            {
                Domain.Entities.HotelRoom room = await context.HotelRooms.FirstOrDefaultAsync(r => r.Id == id);
                return mapper.Map<Domain.Entities.HotelRoom, HotelRoomDto>(room);
            }
            catch
            {
                return null;
            }
        }

        public async Task<HotelRoomDto> UpdateHotelRoom(int id, HotelRoomDto r)
        {
            try
            {
                if (id == r.Id)
                {
                    Domain.Entities.HotelRoom details = await context.HotelRooms.FindAsync(id);
                    // Glorified *patch* via the mapper. This needs to occur before any property updates for EF tracking.
                    var room = mapper.Map(r, details);

                    // We need to let EF know that there's a *new* entity to track.
                    room.UpdatedDate = DateTime.UtcNow;
                    room.UpdatedBy = WindowsIdentity.GetCurrent().Name;

                    var updated = context.Update(room);
                    await context.SaveChangesAsync();

                    return mapper.Map<Domain.Entities.HotelRoom, HotelRoomDto>(updated.Entity);
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public async Task<int> DeleteHotelRoom(int id)
        {
            var details = await context.HotelRooms.FindAsync(id);
            if (details != null)
            {
                context.HotelRooms.Remove(details);
                return await context.SaveChangesAsync();
            }
            else
            {
                return 0;
            }
        }
    }
}
