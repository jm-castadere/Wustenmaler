﻿namespace WusMalBlazorServer.Repository.Contexts
{
    using Microsoft.EntityFrameworkCore;
    using WusMalBlazorServer.Domain.Entities;

    public class BlazorServerDbContext : DbContext
    {
        public DbSet<HotelRoom> HotelRooms { get; set; }

        public BlazorServerDbContext(
            DbContextOptions<BlazorServerDbContext> options) : base(options)
        {
        }
    }
}
