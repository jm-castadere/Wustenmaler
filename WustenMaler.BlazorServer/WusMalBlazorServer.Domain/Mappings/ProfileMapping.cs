﻿namespace WusMalBlazorServer.Domain.Mappings
{
    using AutoMapper;
    using WusMalBlazorServer.Domain.DTOs;

    public class ProfileMapping : Profile
    {
        public ProfileMapping()
        {
            CreateMap<HotelRoomDto, Entities.HotelRoom>().ReverseMap();
        }
    }
}
