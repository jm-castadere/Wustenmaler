﻿namespace WusMalBlazorServer.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using WusMalBlazorServer.Domain.DTOs;

    public interface IBlazorServerRepository
    {
        public Task<HotelRoomDto> CreateHotelRoom(HotelRoomDto r);
        public Task<int> DeleteHotelRoom(int id);
        public IEnumerable<HotelRoomDto> GetAllHotelRooms();
        public Task<HotelRoomDto> GetHotelRoom(int id);
        public Task<HotelRoomDto> IsUnique(string name);
        public Task<HotelRoomDto> UpdateHotelRoom(int id, HotelRoomDto r);
    }
}
